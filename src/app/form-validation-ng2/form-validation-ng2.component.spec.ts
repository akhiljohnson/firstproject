import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormValidationNg2Component } from './form-validation-ng2.component';

describe('FormValidationNg2Component', () => {
  let component: FormValidationNg2Component;
  let fixture: ComponentFixture<FormValidationNg2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormValidationNg2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormValidationNg2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
