import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DoughnutChartComponent, PieChartComponent, BarChartComponent } from 'angular-d3-charts'; // this is needed!

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AppRoutes } from "./app.routing";
import { RouterModule } from "@angular/router";
import { RegisterComponent } from './register/register.component';
import { AccountComponent } from './account/account.component';

import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule} from 'ng2-validation';
import { FormValidationComponent } from './form-validation/form-validation.component';
import { FormValidationNg2Component } from './form-validation-ng2/form-validation-ng2.component';

@NgModule({
  declarations: [
    AppComponent,
    DoughnutChartComponent, 
    PieChartComponent, 
    BarChartComponent, 
    LoginComponent, 
    RegisterComponent, 
    AccountComponent, FormValidationComponent, FormValidationNg2Component,
  ],
  imports: [
    FormsModule,
    CustomFormsModule,
    ReactiveFormsModule,
    BrowserModule,
    RouterModule,
    RouterModule.forRoot(AppRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
