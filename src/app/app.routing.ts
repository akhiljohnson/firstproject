import { AccountComponent } from './account/account.component';
import { Component } from '@angular/core';
import {LoginComponent} from './login/login.component'; 
import { RegisterComponent } from './register/register.component';
import { FormValidationComponent } from './form-validation/form-validation.component';
import { FormValidationNg2Component } from './form-validation-ng2/form-validation-ng2.component';


export const AppRoutes: any = [
    { path: "", component: LoginComponent },
    { path: "registerPage", component: RegisterComponent },
    { path: "accountPage", component: AccountComponent },
    { path: "formVAlidation", component:FormValidationComponent},
    { path: "formng2", component:FormValidationNg2Component}
];

// export const AppComponents: any = [
//     LoginComponent
// ];